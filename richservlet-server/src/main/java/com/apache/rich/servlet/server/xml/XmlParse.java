package com.apache.rich.servlet.server.xml;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;

/**
 * web.xml 解析工具类
 * @author xuanjing.lb
 *
 */
public class XmlParse {
	
	private Map<String, String> servlets = new ConcurrentHashMap<String, String>();
    private Map<String, String> servletmappings = new ConcurrentHashMap<String, String>();
    
    private Map<String, String> filters = new ConcurrentHashMap<String, String>();
    private Map<String, String> filtermappings = new ConcurrentHashMap<String, String>();
    
    
    public XmlParse(){
    		parseXML();
    }
    
    public void parseXML() {

        SAXReader reader = new SAXReader();
        try {
    			String filepath=Thread.currentThread().getClass().getResource("/web.xml").getPath();
            Document doc = reader.read(filepath);
            List<Element> servletlist = doc.selectNodes("/web-app/servlet");
            for (Element servlet : servletlist) {
                Element sname = servlet.element("servlet-name");
                Element sclass = servlet.element("servlet-class");
                servlets.put(sname.getText(), sclass.getText());
            }

            List<Element> servletMappinglist = doc.selectNodes("/web-app/servlet-mapping");
            for (Element servlet : servletMappinglist) {
                Element sname = servlet.element("servlet-name");
                Element url = servlet.element("url-pattern");
                servletmappings.put(url.getText(), sname.getText());
            }
            
            List<Element> filterlist = doc.selectNodes("/web-app/filter");
            for (Element filter : filterlist) {
                Element sname = filter.element("filter-name");
                Element sclass = filter.element("filter-class");
                filters.put(sname.getText(), sclass.getText());
            }

            List<Element> filterMappinglist = doc.selectNodes("/web-app/filter-mapping");
            for (Element filterMapping : filterMappinglist) {
                Element sname = filterMapping.element("filter-name");
                Element url = filterMapping.element("url-pattern");
                filtermappings.put(url.getText(), sname.getText());
            }

        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }
    

	/**
	 * @return the mappings
	 */
	public Map<String, String> getServletmappings() {
		return servletmappings;
	}
	
	
	/**
	 * @return the filtermappings
	 */
	public Map<String, String> getFiltermappings() {
		return filtermappings;
	}

	/**
	 * @param servletmappings the servletmappings to set
	 */
	public void setServletmappings(Map<String, String> servletmappings) {
		this.servletmappings = servletmappings;
	}

	/**
     * 通过 url 找到 类名
     * 
     * @param url
     * @return
     */
    public String getServletClassByUrl(String url) {
        String cname = null;
        String sname = servletmappings.get(url);
        cname = servlets.get(sname);
        return cname;

    }

	public static void main(String[] args) {
		String path=Thread.currentThread().getClass().getResource("/web.xml").getPath();
		System.out.println(new XmlParse());
	}
}
