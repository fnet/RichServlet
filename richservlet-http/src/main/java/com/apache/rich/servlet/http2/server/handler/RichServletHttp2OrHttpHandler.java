/**
 * 
 */
package com.apache.rich.servlet.http2.server.handler;

import java.util.concurrent.TimeUnit;

import com.alibaba.fastjson.JSONObject;

import io.netty.handler.ssl.SslHandler;
import io.netty.handler.ssl.SslHandshakeCompletionEvent;
import com.apache.rich.servlet.http.server.handler.RichServletAsyncHttpRequestHandler;
import io.netty.handler.codec.http2.DefaultHttp2Connection;
import io.netty.handler.codec.http2.HttpToHttp2ConnectionHandlerBuilder;
import io.netty.handler.codec.http2.InboundHttp2ToHttpAdapter;
import io.netty.handler.codec.http2.InboundHttp2ToHttpAdapterBuilder;
import com.apache.rich.servlet.core.server.helper.RichServletServerOptionProvider;
import com.apache.rich.servlet.core.server.helper.RichServletServerOptions;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.ssl.ApplicationProtocolNames;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.ssl.ApplicationProtocolNegotiationHandler;

/**
 * @author wanghailing
 *
 */
public class RichServletHttp2OrHttpHandler extends ApplicationProtocolNegotiationHandler {
	
	private RichServletServerOptionProvider options;
	
	public RichServletHttp2OrHttpHandler(RichServletServerOptionProvider options) {
        super(ApplicationProtocolNames.HTTP_1_1);
        this.options=options;
	}

	/* (non-Javadoc)
	 * @see io.netty.handler.ssl.ApplicationProtocolNegotiationHandler#configurePipeline(io.netty.channel.ChannelHandlerContext, java.lang.String)
	 */
	@Override
	protected void configurePipeline(ChannelHandlerContext ctx, String protocol)
			throws Exception {
		
		if (ApplicationProtocolNames.HTTP_2.equals(protocol)) {
			 	DefaultHttp2Connection connection = new DefaultHttp2Connection(true);
		        InboundHttp2ToHttpAdapter listener = new InboundHttp2ToHttpAdapterBuilder(connection)
		                .propagateSettings(true)
		                .maxContentLength(options.option(RichServletServerOptions.HTTP_MAX_CONTENT_LENGTH)).build();

		        ctx.pipeline().addLast(new HttpToHttp2ConnectionHandlerBuilder()
		                .frameListener(listener)
		                .connection(connection).build());
		        ctx.pipeline().addLast("richservlet-http2-request-poster", RichServletAsyncHttp2RequestHandler.build());
		        return;
		}else if(ApplicationProtocolNames.HTTP_1_1.equals(protocol)){
			ctx.pipeline().addLast("richservlet-timer", new ReadTimeoutHandler(options.option(RichServletServerOptions.TCP_TIMEOUT), TimeUnit.MILLISECONDS))
    				.addLast("richservlet-http-decoder", new HttpRequestDecoder())
    				.addLast("richservlet-http-aggregator", new HttpObjectAggregator(options.option(RichServletServerOptions.MAX_PACKET_SIZE)))
    				.addLast("richservlet-http-request-poster", RichServletAsyncHttpRequestHandler.build())
    				.addLast("richservlet-http-encoder", new HttpResponseEncoder());
			return;
		}
		throw new IllegalStateException("unknown protocol: " + protocol);
	}
	
	
}
