/**
 * 
 */
package com.apache.rich.servlet.http.servlet.realize.configuration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import com.apache.rich.servlet.http.servlet.realize.RichServletHttpServletContext;

import com.apache.rich.servlet.http.servlet.realize.adapter.ConfigAdapter;

/**
 * @author wanghailing
 *
 */
public class RichServletHttpServletConfig extends ConfigAdapter implements
		ServletConfig {

	public RichServletHttpServletConfig(String ownerName) {
		super(ownerName);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletConfig#getServletName()
	 */
	@Override
	public String getServletName() {
		return super.getOwnerName();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletConfig#getServletContext()
	 */
	@Override
	public ServletContext getServletContext() {
		return RichServletHttpServletContext.get();
	}

}
