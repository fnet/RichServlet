
package com.apache.rich.servlet.http.servlet.realize.session;

import com.apache.rich.servlet.http.servlet.realize.RichServletHttpServletWebapp;



public class HttpSessionThreadLocal {

    public static final ThreadLocal<RichServletHttpSession> sessionThreadLocal = new ThreadLocal<RichServletHttpSession>();

    private static RichServletHttpSessionStore sessionStore;

    public static RichServletHttpSessionStore getSessionStore() {
        return sessionStore;
    }

    public static void setSessionStore(RichServletHttpSessionStore store) {
        sessionStore = store;
    }

    public static void set(RichServletHttpSession session) {
        sessionThreadLocal.set(session);
    }

    public static void unset() {
        sessionThreadLocal.remove();
    }

    public static RichServletHttpSession get() {
    		RichServletHttpSession session = sessionThreadLocal.get();
        if (session != null)
            session.touch();
        return session;
    }

    public static RichServletHttpSession getOrCreate() {
        if (HttpSessionThreadLocal.get() == null) {
            if (sessionStore == null) {
                sessionStore = new RichServletHttpSessionStoreImpl();
            }

            RichServletHttpSession newSession = sessionStore.createSession();
            newSession.setMaxInactiveInterval(RichServletHttpServletWebapp.get()
                    .getWebappConfig().getSessionTimeout());
            sessionThreadLocal.set(sessionStore.createSession());
        }
        return get();
    }

}
