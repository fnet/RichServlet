
package com.apache.rich.servlet.http.servlet.realize.interceptor;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;

/**
 * servlet 拦截器接口
 * @author wanghailing
 *
 */
public interface RichServletInterceptor {

    void onRequestReceived(ChannelHandlerContext ctx, HttpRequest e);

    void onRequestSuccessed(ChannelHandlerContext ctx, HttpRequest e,
                            HttpResponse response);

    void onRequestFailed(ChannelHandlerContext ctx, Throwable e,
                         HttpResponse response);

}
