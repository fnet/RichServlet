/**
 * 
 */
package com.apache.rich.servlet.core.server.helper;

/**
 * @author wanghailing
 *
 */
import java.util.concurrent.TimeUnit;

public class RichServletServerOptions<T> {

    // default value for every option
    private final T defaultValue;

    public RichServletServerOptions(T defaultValue) {
        this.defaultValue = defaultValue;
    }

    public T defaultValue() {
        return defaultValue;
    }

    /**
     * max tcp accept compulish queue.
     *
     * NOT effect on max connections restriction. just a hint value for linux listen(fd, backlog)
     * increase the number if you have lots of short connection which sockets will be filled
     * in tcp accept compulish queue too much.
     *
     */
    public static final RichServletServerOptions<Integer> TCP_BACKLOG = new RichServletServerOptions<>(1024);

    /**
     * max connections in this httpserver instance. default is 8192
     *
     * incoming http connection over the number of maxConnections will be reject
     * and return http code 503
     */
    public static final RichServletServerOptions<Integer> MAX_CONNETIONS = new RichServletServerOptions<>(8192);

    /**
     * max received packet size. default is 16MB
     */
    public static final RichServletServerOptions<Integer> MAX_PACKET_SIZE = new RichServletServerOptions<>(16 * 1024 * 1024);

    /**
     * network socket io threads number. default is cpu core - 1
     *
     * NOT set the number more than cpu core number
     */
    public static final RichServletServerOptions<Integer> IO_THREADS = new RichServletServerOptions<>(Runtime.getRuntime().availableProcessors() - 1);

    /**
     * logic handler threads number. default is 128
     *
     * we suggest adjust the ioThreads number bigger if there are more. critical region
     * code or block code in your handler logic (io intensive). and smaller if your code
     * has no block almost (cpu intensive)
     */
    public static final RichServletServerOptions<Integer> WORKER_THREADS = new RichServletServerOptions<>(128);

    /**
     * logic handler timeout. default is 30s
     */
    public static final RichServletServerOptions<Long> TCP_TIMEOUT = new RichServletServerOptions<>((Long) TimeUnit.SECONDS.toMillis(30));


    public static final RichServletServerOptions<Boolean> TCP_NODELAY = new RichServletServerOptions<>(Boolean.TRUE);

    public static final RichServletServerOptions<String> ACCESS_LOG = new RichServletServerOptions<>("");

    public static final RichServletServerOptions<Boolean> ACCESS_LOG_BUFFER_IO = new RichServletServerOptions<>(false);
    
    public static final RichServletServerOptions<String> TCP_HOST = new RichServletServerOptions<>("0.0.0.0");

    public static final RichServletServerOptions<Integer> TCP_PORT = new RichServletServerOptions<>(8080);
    //http报文最大长度 ,default  1M
    public static final RichServletServerOptions<Integer> HTTP_MAX_CONTENT_LENGTH = new RichServletServerOptions<>(2048 * 100);

}

