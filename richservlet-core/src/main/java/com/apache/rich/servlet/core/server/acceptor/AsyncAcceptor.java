package com.apache.rich.servlet.core.server.acceptor;


import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import com.apache.rich.servlet.core.server.helper.RichServletServerOptionProvider;
import com.apache.rich.servlet.core.server.helper.RichServletServerOptions;
import com.apache.rich.servlet.core.server.RichServletServer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;


/**
 * 
 * @author wanghailing
 *
 */
public abstract class AsyncAcceptor implements IOAcceptor {

    
	private  static final  Logger logger = LoggerFactory.getLogger(AsyncAcceptor.class);

    private final RichServletServer richServletServer;


    private ChannelFuture serverSocket;
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;

    public AsyncAcceptor(RichServletServer richServletServer) {
        
        this.richServletServer = richServletServer;
    }

    @Override
    public void eventLoop() {

        // accept threads indicated the threads only work on accept() syscall,
        // receive client socket then transfor to io thread pool. bigger pool size
        // conform in SHORT CONNECTION scene (alse HTTP)
        int acceptThreads = Math.max(2, ((int) (richServletServer.option(RichServletServerOptions.IO_THREADS) * 0.3) & 0xFFFFFFFE));


        // io threads (worker threads) handle client socket's read(), write()
        // and "business logic flow". in http short connection protocol socket's
        // read(), write() only a few times.
        //
        // HttpAdapter 's "business logic flow" has params building, validation,
        // logging , so we increase this pool percent in MaxThreads
        int rwThreads = Math.max(4, ((int) (richServletServer.option(RichServletServerOptions.IO_THREADS) * 0.7) & 0xFFFFFFFE));

        bossGroup = new NioEventLoopGroup(acceptThreads);
        workerGroup = new NioEventLoopGroup(rwThreads);

        // initial request router's work threads
        AsyncRequestHandler.newTaskPool(richServletServer.option(RichServletServerOptions.WORKER_THREADS));
        AsyncRequestHandler.useURLResourceController(richServletServer.getRouteController());
        AsyncRequestHandler.useInterceptor(richServletServer.getInterceptor());

        ServerBootstrap socketServer = new ServerBootstrap();
        socketServer.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        protocolPipeline(ch, richServletServer);
                    }
                })
                .option(ChannelOption.SO_BACKLOG, richServletServer.option(RichServletServerOptions.TCP_BACKLOG))
                //.option(ChannelOption.SO_TIMEOUT, richServletServer.option(RichServletServerOptions.TCP_TIMEOUT))
                .option(ChannelOption.SO_REUSEADDR, true)
                .childOption(ChannelOption.TCP_NODELAY, richServletServer.option(RichServletServerOptions.TCP_NODELAY))
                .childOption(ChannelOption.SO_KEEPALIVE, true);
        // Bind and start to accept incoming connections.
        try {
            serverSocket = socketServer.bind(richServletServer.option(RichServletServerOptions.TCP_HOST), richServletServer.option(RichServletServerOptions.TCP_PORT)).sync();
        } catch (InterruptedException ignored) {
        		ignored.printStackTrace();
        		logger.error("AsyncAcceptor eventLoop error,errorMsg={},e={}",ignored.getMessage(),ignored);
        }
    }

    @Override
    public void join() throws InterruptedException {
        // sync waitting until the server socket is closed.
        serverSocket.channel().closeFuture().sync();
    }

    @Override
    public void shutdown() {
        workerGroup.shutdownGracefully();
        bossGroup.shutdownGracefully();
    }

    protected abstract void protocolPipeline(SocketChannel ch, RichServletServerOptionProvider options) throws Exception;
}
