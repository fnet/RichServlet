package com.apache.rich.servlet.core.server.rest.response;

import com.apache.rich.servlet.common.enums.ResultCodeEnums;


/**
 * http结果封装
 * @author wanghailing
 *
 */
public class HttpResponseResult {

    private ResultCodeEnums httpStatus;
    
    private Object httpContent;

    public HttpResponseResult(ResultCodeEnums status) {
        this(status, null);
    }

    public HttpResponseResult(ResultCodeEnums status, Object content) {
        this.httpStatus = status;
        this.httpContent = content;
    }

    public ResultCodeEnums getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(ResultCodeEnums httpStatus) {
        this.httpStatus = httpStatus;
    }

    public Object getHttpContent() {
        return httpContent;
    }

    public void setHttpContent(Object httpContent) {
        this.httpContent = httpContent;
    }
}
