package com.apache.rich.servlet.core.server.rest.controller;


import java.lang.reflect.Method;

import com.apache.rich.servlet.core.server.rest.response.HttpResponseResult;

import com.apache.rich.servlet.core.server.utils.Hitcounter;
import com.apache.rich.servlet.core.server.rest.HttpContext;
import com.apache.rich.servlet.common.exception.HServerException;
import com.apache.rich.servlet.common.enums.ResultCodeEnums;

/**
 * 
 * @author wanghailing
 *
 */
public class URLController extends Hitcounter {

    // target class
    private ControllerClassDescriptor provider;
    // target method
    private ControllerMethodDescriptor procedure;
    // internal controller sign. only for root-path
    private boolean internal = false;

    private URLController() {

    }

    public static URLController fromProvider(String URI, Class<?> providerClass, Method procedure) {
        URLController handler = new URLController();
        handler.provider = new ControllerClassDescriptor(providerClass);
        handler.procedure = new ControllerMethodDescriptor(URI, handler.provider, procedure);
        return handler;
    }

    public URLController internal() {
        this.internal = true;
        return this;
    }

    public boolean isInternal() {
        return this.internal;
    }

    public HttpResponseResult call(HttpContext context) {
        /**
         * make new controller class instance with every http request. because
         * of we desire every request may has own context variables and status.
         *
         * TODO : This newInstance() need a empty param default constructor.
         *
         */

        try {
            Object result = procedure.invoke(context);
            if (result != null && !result.getClass().isPrimitive())
                return new HttpResponseResult(ResultCodeEnums.SUCCESS, result);
            else
                return new HttpResponseResult(ResultCodeEnums.RESPONSE_NOT_VALID);
        } catch (HServerException e) {
            return new HttpResponseResult(ResultCodeEnums.PARAMS_NOT_MATCHED);
        }
    }

    @Override
    public String toString() {
        return String.format("provider=%s, procedure=%s", provider.getClazz().getCanonicalName(), procedure.getMethod().getName());
    }
}
